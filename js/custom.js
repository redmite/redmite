(function(){
  "use strict";  
  	var redmite = redmite || {};
	redmite.namespace = function (ns_string) {
		var parts = ns_string.split('.'),
		parent = redmite,
		i;	
	  	if (parts[0] === 'redmite') {
			parts = parts.slice(1);
	  	}	
	  	for (i = 0; i < parts.length; i += 1) {
			if (typeof parent[parts[i]] === 'undefined') {
				parent[parts[i]] = {};
			}
			parent = parent[parts[i]];
	  	}	
	  	return parent;
	};	
	redmite.core = {
		init: function(){
			this.initCustomSelect();
		},
		initCustomSelect: function(){
			$('.select-custom').each(function(){
				var $this = $(this), numberOfOptions = $(this).children('option').length;
			  
				$this.addClass('select-hidden'); 
				$this.wrap('<div class="select"></div>');
				$this.after('<div class="select-styled"></div>');
		
				var $styledSelect = $this.next('div.select-styled');
				$styledSelect.text($this.children('option').eq(0).text());
			  
				var $list = $('<ul />', {
					'class': 'select-options'
				}).insertAfter($styledSelect);
			  
				for (var i = 0; i < numberOfOptions; i++) {
					$('<li />', {
						text: $this.children('option').eq(i).text(),
						rel: $this.children('option').eq(i).val()
					}).appendTo($list);
				}
			  
				var $listItems = $list.children('li');
			  
				$styledSelect.click(function(e) {
					e.stopPropagation();
					$('div.select-styled.active').not(this).each(function(){
						$(this).removeClass('active').next('ul.select-options').hide();
					});
					$(this).toggleClass('active').next('ul.select-options').toggle();
				});
			  
				$listItems.click(function(e) {
					e.stopPropagation();
					$styledSelect.text($(this).text()).removeClass('active');
					$this.val($(this).attr('rel'));
					$list.hide();
				});
			  
				$(document).click(function() {
					$styledSelect.removeClass('active');
					$list.hide();
				});
		
			});
		}
	};	
	$(document).ready(function() {
		redmite.core.init();	
		$('#reportTable').DataTable({
			searching:  false,
			ordering:  false,
			"lengthMenu": [[10, 20, 30], [10, 20, 30]],
			"pagingType": "simple_numbers",
			"language": {
            	"lengthMenu": "Results per page _MENU_",
				"info": "Showing _PAGE_ of _PAGES_ entries"
			}
		});
	});  	
})();